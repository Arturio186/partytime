const monthNames = [
    'января', 'февраля', 'марта',
    'апреля', 'мая', 'июня',
    'июля', 'августа', 'сентября',
    'октября', 'ноября', 'декабря',
]

export const convertDate = (date : string) : Array<string> => {
    const elements = date.split('.');

    return [elements[0], monthNames[+elements[1]]];
}