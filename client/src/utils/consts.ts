export const LOGIN_ROUTE : string = '/login';
export const REGISTER_ROUTE : string = '/register';

export const MESSAGE_ROUTE : string = '/im';
export const PROFILE_ROUTE : string = '/profile';
export const FRIENDS_ROUTE : string = '/friends';
export const MAP_ROUTE : string = '/map';
export const PARTIES_ROUTE : string = '/parties';




