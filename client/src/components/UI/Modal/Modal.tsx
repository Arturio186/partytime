import React, {FC, ReactNode, useState} from 'react';
import classes from './Modal.module.scss';

interface IModalProps {
    children: ReactNode;
    visible: boolean;
    setVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

const Modal : FC<IModalProps> = ({children, visible, setVisible}) => {
    const rootClasses = [classes.modal];
    
    if (visible) {
        rootClasses.push(classes.active);
    }

    return (
        <div className={rootClasses.join(' ') } onClick={() => setVisible(false)}>
            <div className={classes.modalContent} onClick={(event) => event.stopPropagation()}>
                {children}
            </div>
        </div>
    )
}

export default Modal;
