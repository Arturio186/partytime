import React, {FC, useEffect} from 'react';
import { Link, useLocation } from 'react-router-dom';
import { privateRoutes } from '../../../routes';
import classes from './NavBar.module.scss';

const NavBar : FC = () => {
    const location = useLocation();
    const currentPath = location.pathname;

    useEffect(()=> {
        const result = privateRoutes.filter((route)=> {
            return route.path === currentPath;
        });
        document.title = result[0].title;
    }, [currentPath]);

    return (
        <div className={classes.content}>
            {privateRoutes.map(route => 
                <Link
                    key={route.path}
                    to={route.path}
                    className={currentPath === route.path ? classes.current : ''}
                >
                    {route.title}
                </Link>
            )}
        
        </div>
    )
}

export default NavBar;