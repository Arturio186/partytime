import React, {FC, ReactNode} from 'react';

import classes from './AuthButton.module.scss';

interface IAuthButtonProps {
    children: ReactNode;
}
const AuthButton : FC<IAuthButtonProps> = ({children}) => {
    return (
        <button className={classes.authButton}>{children}</button>
    )
}

export default AuthButton;