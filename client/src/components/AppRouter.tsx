import React, { FC } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { publicRoutes, privateRoutes } from '../routes';
import { LOGIN_ROUTE, MESSAGE_ROUTE } from '../utils/consts';
import NavBar from './UI/NavBar/NavBar';

const AppRouter : FC = () => {
    const isAuth : boolean = true;

    return (
        isAuth ?
        <div className="content">
            <header>
                <p className="title">PartyTime</p>
                <img src="./images/logout.svg" alt="logout" className="logout" />
            </header>
            <div className="main">
                <NavBar/>
                <Routes>
                    {privateRoutes.map((route) => 
                        <Route
                            path={route.path}
                            Component={route.component}
                            key={route.path}
                        />
                    )}
                    <Route path="/*" element={<Navigate to={MESSAGE_ROUTE}/>}/>
                </Routes>
            </div>
        </div>
        
        :
        <Routes>
            {publicRoutes.map((route) => 
                <Route
                    path={route.path}
                    Component={route.component}
                    key={route.path}
                />
            )}
            <Route path="/*" element={<Navigate to={LOGIN_ROUTE}/>}/>
        </Routes>
    )
}

export default AppRouter;