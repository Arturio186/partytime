import React, {FC, ReactNode} from 'react';

import classes from './PartyBalloon.module.scss';
import { convertDate } from '../../utils/convertDate';

interface IPartyInfo {
    x: number;
    y: number;
    title: string;
    time: string;
    date: string;
    description: string;
}

interface IPartyBalloonProps {
    party: IPartyInfo;
}

const PartyBalloon : FC<IPartyBalloonProps> = ({party}) => {
    const [day, month] = convertDate(party.date);

    return (
        <div className={classes.balloon}>
            <div className={classes.title}>
                <img src="./images/man.svg" alt="man"/>
                <p className={classes.title}>{party.title}</p>
            </div>
            <p className={classes.creator}>Артур Гаджиибрагимов</p>
            <div className={classes.time}>
                <div className={classes.date}>
                    <p className={classes.day}>{day}</p>
                    <p className={classes.month}>{month}</p>
                </div>
                <span className={classes.line}></span>
                <p className={classes.clock}>{party.time}</p>
            </div>
            <p className={classes.description}>{party.description}</p>
            
            <button className={classes.link}>На страницу тусовки</button>
        </div>
    )
}

export default PartyBalloon;