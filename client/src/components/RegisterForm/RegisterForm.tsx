import React, {FC} from 'react';
import { useForm, SubmitHandler} from 'react-hook-form';
import { NavLink } from 'react-router-dom';
import { LOGIN_ROUTE } from '../../utils/consts';

import AuthButton from '../UI/AuthButton/AuthButton';
import AuthInput from '../UI/AuthInput/AuthInput';

import classes from './RegisterForm.module.scss';

interface IRegisterField {
    email: string
    password: string
    confirmPassword: string
}

const RegisterForm : FC = () => {
    const {watch, register, handleSubmit, formState: { errors }} = useForm<IRegisterField>({mode: "onChange"})

    const onSubmit: SubmitHandler<IRegisterField> = (data) => {
        console.log(data)
    }

    const password = watch("password");

    return (
        <div className={classes.content}>
            <div className={classes.logo}>
                <img src="./images/logo.svg" alt="logo"/>
            </div>

            <h1 className={classes.title}>Регистрация</h1>

            <form onSubmit={handleSubmit(onSubmit)} className={classes.container}>
                <AuthInput
                    placeholder="Email"
                    type="text"
                    register={register('email', {
                        required: 'Введите email адрес',
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: 'Некорректный email адрес'
                        },
                    })}
                    error={errors.email}
                />
                <AuthInput
                    placeholder="Пароль"
                    type="password"
                    register={register('password', {
                        required: 'Введите пароль',
                        minLength: {
                            value: 6,
                            message: 'Пароль должен состоять минимум из 6 символов'
                        }
                    })}
                    error={errors.password}
                />
                <AuthInput
                    placeholder="Повторите пароль"
                    type="password"
                    register={register('confirmPassword', {
                        required: 'Повторите пароль',
                        validate: (value: string) => {
                            return password === value || "Пароль не совпадает";
                          }, 
                    })}
                    error={errors.confirmPassword}
                />
                <AuthButton>Зарегистрировоаться</AuthButton>
            </form>

            <p className={classes.link}>Уже есть аккаунт? <NavLink to={LOGIN_ROUTE}>Войдите!</NavLink></p>
        </div>
        
    )
}

export default RegisterForm;