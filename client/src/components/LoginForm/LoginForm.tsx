import React, {FC} from 'react';
import { useForm, SubmitHandler} from 'react-hook-form';
import { NavLink } from 'react-router-dom';
import { REGISTER_ROUTE } from '../../utils/consts';

import AuthButton from '../UI/AuthButton/AuthButton';
import AuthInput from '../UI/AuthInput/AuthInput';

import classes from './LoginForm.module.scss';

interface ILoginField {
    email: string
    password: string
}

const LoginForm : FC = () => {
    const {register, handleSubmit, formState: { errors }} = useForm<ILoginField>({mode: "onChange"})

    const onSubmit: SubmitHandler<ILoginField> = (data) => {
        console.log(data)
    }

    return (
        <div className={classes.content}>
            <div className={classes.logo}>
                <img src="./images/logo.svg" alt="logo"/>
            </div>

            <h1 className={classes.title}>Авторизация</h1>

            <form onSubmit={handleSubmit(onSubmit)} className={classes.container}>
                <AuthInput
                    placeholder="Email"
                    type="text"
                    register={register('email', {
                        required: 'Введите email адрес',
                        pattern: {
                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                            message: 'Некорректный email адрес'
                        },
                    })}
                    error={errors.email}
                />
                <AuthInput
                    placeholder="Пароль"
                    type="password"
                    register={register('password', {
                        required: 'Введите пароль',
                        minLength: {
                            value: 6,
                            message: 'Пароль должен состоять минимум из 6 символов'
                        }
                    })}
                    error={errors.password}
                    
                />
                <AuthButton>Войти</AuthButton>
            </form>

            <p className={classes.link}>Нет аккаунта? <NavLink to={REGISTER_ROUTE}>Зарегистрируйтесь!</NavLink></p>
        </div>
        
    )
}

export default LoginForm;