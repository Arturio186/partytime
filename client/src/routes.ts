import { FC } from "react";

import { LOGIN_ROUTE, REGISTER_ROUTE, 
        MESSAGE_ROUTE, PROFILE_ROUTE, MAP_ROUTE, 
        FRIENDS_ROUTE, PARTIES_ROUTE } from "./utils/consts";

import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";

import Messages from "./pages/Messages/Messages";
import Profile from "./pages/Profile/Profile";
import Friends from "./pages/Friends/Friends";
import PartyMap from "./pages/PartyMap/PartyMap";
import Parties from "./pages/Parties/Parties";

interface IRoute {
    path: string;
    component: FC;
    title: string;
}

interface IRoutes extends Array<IRoute> {}

export const publicRoutes : IRoutes = [
    {path: LOGIN_ROUTE, component: Login, title: "Авторизация"},
    {path: REGISTER_ROUTE, component: Register, title: "Регистрация"},
];

export const privateRoutes : IRoutes = [
    {path: PROFILE_ROUTE, component: Profile, title: "Профиль"},
    {path: MESSAGE_ROUTE, component: Messages, title: "Сообщения"},
    {path: FRIENDS_ROUTE, component: Friends, title: "Друзья"},
    {path: MAP_ROUTE, component: PartyMap, title: "Карта"},
    {path: PARTIES_ROUTE, component: Parties, title: "Вечеринки"}
];