import React, { FC } from 'react';

import './Login.scss';
import LoginForm from '../../components/LoginForm/LoginForm';


const Login : FC = () => {
    document.title = "Аутентификация";
    return (
        <div className="page">
            <LoginForm/>
        </div>
    )
}

export default Login;