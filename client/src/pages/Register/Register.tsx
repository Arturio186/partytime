import React, { FC } from 'react';

import './Register.scss';
import RegisterForm from '../../components/RegisterForm/RegisterForm';


const Register : FC = () => {
    document.title = "Регистрация";
    return (
        <div className="page">
            <RegisterForm/>
        </div>
    )
}

export default Register;