import React, {FC, useRef, useState} from 'react';
import {YMaps, Map, Placemark, Clusterer, Button} from 'react-yandex-maps';
import {renderToString} from 'react-dom/server';
import './PartyMap.scss';

import './PartyMap.scss';
import { NamedImportBindings } from 'typescript';
import PartyBalloon from '../../components/PartyBalloon/PartyBalloon';
import Modal from '../../components/UI/Modal/Modal';
import CreatePartyForm from '../../components/CreatePartyForm/CreatePartyForm';

interface IPartyInfo {
    x: number;
    y: number;
    title: string;
    time: string;
    date: string;
    description: string;
}

const PartyMap : FC = () => {
    const [modalVisible,  setModalVisible] = useState(false);

    const mapRef = useRef<any>(null);
    const creatorPlacemarkRef = useRef<any>(null);

    const [creatorPlacemark, setCreatorPlacemark] = useState<React.ReactNode>(null);

    const markers : Array<IPartyInfo> = [
        {x: 57.1493, y: 65.5412, title: 'Посвящение в студенты', time: '21:30', date: '21.10.2023', description: 'Тусовка для тех, кто только начал учиться!'},
        {x: 57.1593, y: 65.5412, title: 'Посвящение в студенты', time: '21:30', date: '21.10.2023', description: 'Тусовка для тех, кто только начал учиться!'},
        {x: 57.1693, y: 65.5412, title: 'Посвящение в студенты', time: '21:30', date: '21.10.2023', description: 'Тусовка для тех, кто только начал учиться!'},
        {x: 57.1793, y: 65.5412, title: 'Посвящение в студенты', time: '21:30', date: '21.10.2023', description: 'Тусовка для тех, кто только начал учиться!'},

    ];

    const addCreatorPlacemark = () => {
        const focusCoord = mapRef.current.getCenter();
        
        setCreatorPlacemark(<Placemark
            onClick={() => setModalVisible(true)}
            instanceRef={(placemark : any) => creatorPlacemarkRef.current = placemark}
            geometry={{
                type: 'Point',
                coordinates: focusCoord
            }}
            options={{
                preset: 'islands#dotIcon',
                iconColor: 'red',
                draggable: true
            }} 
            properties={{
                hintContent: 'Нажми на меня!'
            }}
        />)
    }

    return (
        <div className="inner-content">
            <h1>Интерактивная карта тусовок</h1>
            <div className="map">
                <YMaps query={{
                    apikey: '0b375996-25a4-4d5d-9152-504fa8810cd2',
                }}>
                    <Map 
                        width="95%"
                        height="80%"
                        modules={ [ 'geoObject.addon.balloon', 'geoObject.addon.hint' ] }
                        defaultState={{center: [57.1493, 65.5412], zoom: 15}}
                        instanceRef={(map : any) => mapRef.current = map}
                    >
                        <Button
                            options={{ maxWidth: 128, selectOnClick: false }}
                            data={{ content: "Создать тусовку" }}
                            onClick={addCreatorPlacemark}
                        />
                        {creatorPlacemark}
                        <Clusterer
                            options={{
                                preset: "islands#invertedBlueClusterIcons",
                                groupByCoordinates: false,
                            }}
                        >
                            {markers.map((marker) => {
                                return <Placemark
                                    geometry={{
                                        type: 'Point',
                                        coordinates: [marker.x, marker.y]
                                    }}
                                    options={{
                                        preset: 'islands#dotIcon',
                                        iconColor: 'blue',
                                    }} 
                                    properties={{
                                        hintContent: 'Вечеринка',
                                        balloonContent: renderToString(<PartyBalloon party={marker}/>)
                                    }}
                                />
                            })}
                        </Clusterer>
                    </Map>
                </YMaps>
                <Modal visible={modalVisible} setVisible={setModalVisible}>
                    <CreatePartyForm/>
                </Modal>
            </div>
        </div>
    )
}

export default PartyMap;