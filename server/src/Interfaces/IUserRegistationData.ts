export default interface IUserRegistationData {
    surname: string;
    name: string;
    email: string;
    password: string;
    birth_date: string;
    gender: string
}