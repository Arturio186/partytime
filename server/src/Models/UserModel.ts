import db from "../Database/knex";
import IUserRegistationData from "../Interfaces/IUserRegistationData";

class UserModel {
    static async Create (data : IUserRegistationData) {
        const [user] = await db('users')
            .returning('*')
            .insert({ 
                surname: data.surname,  
                name: data.name,
                email: data.email,
                password: data.password,
                birth_date: data.birth_date,
                gender: data.gender
            });

        return user;
    }
}
